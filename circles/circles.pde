Circle[] circs;
Circle[] bigCircs;
int numOfBigCircs = 0;
int numOfCircs = 0;
float radius;
float bigRadius;
color col;
color colTwo;
color a = #570027;
color b = #EE2A61;
color c = #EF5174;
color d = #F18295;
color e = #8E227E;
color f = #FC3153;
color g = #FF7942;
color h = #FF9F3A;
color j = #FF3254;
color y = #EECB2D;

color aa = #092846;
color bb = #00BCE4;
color cc = #70CCEA;
color dd = #D4ECF7;
color ee = #66BD62;
color ff = #66BD62;

color darkPurp =  #2D183D;
color lightPurp = #580F23;
color midPurp = #562355; 
color bgPurpl = #18183d;
color[] hexs = { a, b, c, d, e, f, g, h, j, aa,bb,cc,dd,ee,ff, y};
color[] blueHexs = { aa,bb,cc,dd,ee,ff, a, b, c };
color[] purpHexs = { darkPurp, bgPurpl, bgPurpl, bgPurpl, bgPurpl, darkPurp, midPurp, bgPurpl, bgPurpl, darkPurp, bgPurpl, darkPurp };

void mouseClicked() {
  saveFrame("image####.png");
}

void setup() {
  colorMode(HSB, height, height, height);
  smooth();
  size(700, 934);
  background(#18183d);
  noLoop();
  numOfCircs = width / 11;
  numOfBigCircs = width / 2;
  radius = 2.0;
  bigRadius = 2.0;
  circs = new Circle[numOfCircs];
  bigCircs = new Circle[numOfBigCircs];
  
  noStroke();
  fill(255, 0);
  for (int j = 0; j < numOfBigCircs; j = j+1) {
    bigCircs[j] = new Circle(bigRadius);
    bigCircs[j].configure();
    bigRadius = bigRadius + 5.0;
  }
  for (int i = 0; i < numOfCircs; i = i+1) {
    circs[i] = new Circle(radius);
    circs[i].configure();
    radius = radius + 5.0;
  }
}

void draw() {
  for (Circle bigCirc : bigCircs) {
    for(int j=0;j<bigCirc.numOfPoints;j++) {
      float x = bigCirc.xyArray[j][0];
      float y = bigCirc.xyArray[j][1];
      noStroke();
      PShape point = createShape(ELLIPSE, x, y, 4, 4);
      col =  purpHexs[ int(random(0,purpHexs.length)) ];
      point.setFill(col);
      point.setStroke(false);
      shape(point);
    }
  }
  
  for (Circle circ : circs) {
    for(int i=0;i<circ.numOfPoints;i++) {
      float x = circ.xyArray[i][0];
      float y = circ.xyArray[i][1];
      noStroke();
      PShape point = createShape(ELLIPSE, x, y, 4, 4);
      col = hexs[ int(random(0,hexs.length)) ];
      point.setFill(col);
      point.setStroke(false);
      shape(point);
    }
  }
}

class Circle {
  float radius;
  float angle;
  int numOfPoints;
  float[][] xyArray;
  float noiseScale = .006;
  float noiseAmount;
  int amplify;
  
  Circle(float tmpRadius) { 
    radius = tmpRadius;
    numOfPoints = int(radius * 1.2);
    println(radius);
  }
  
  void configure() {
    int amplify = int(random(0, 0));
    angle = TWO_PI/(float)numOfPoints;
    xyArray = new float [numOfPoints][3];
    for(int i=0;i<numOfPoints;i++) {       
      float x = radius*sin(angle*i)+width/2;
      float y = radius*cos(angle*i)+height/2;
      noiseAmount = map(amplify,0,width,0,30);
      x = x + map(noise(noiseScale*x,noiseScale*y,0),0,1,-noiseAmount,noiseAmount);
      y = y + map(noise(noiseScale*y,noiseScale*y,1),0,1,-noiseAmount,noiseAmount);
      xyArray[i][0] = x;
      xyArray[i][1] = y;
      noiseScale += 0.00000005;
    }
  }
  
}