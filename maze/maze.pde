import geomerative.*;

int p = 5;
RShape gShape;
RPoint topLeft, bottomRight;
float x;
float y;
float inc = 2.5;
float s = 1.5;
color a = #91237c;
color b = #3175b6;
color j = #5e1c8f;


void setup() {
  size(800, 800);
  //background(#18183d);
  background(255, 0);
  smooth();
  frameRate(100);
  noLoop();
  RG.init(this); 
  RCommand.setSegmentLength(10);
  RCommand.setSegmentator(RCommand.UNIFORMLENGTH);
  gShape = RG.loadShape("circle.svg");
  getBoundaries();
  x = random(topLeft.x, bottomRight.x);
  y = random(topLeft.y, bottomRight.y);
  
}

void draw() {
  for(int y=0; y <height; y+=p){
    for(int x =0; x < width; x +=p){
      boolean isInShape = gShape.contains(new RPoint(x, y));
      boolean isInShapeTwo = gShape.contains(new RPoint(x+p, y+p));
      if(isInShape && isInShapeTwo) {
        stroke(0);
        strokeWeight(2);
        float n = noise(x * inc, y * inc) * s;
        if(random(1)>0.5){
          line(x,y+n,x+p,y+p);
        } else {
          line(x,y+p+n,x+p,y);
        }
      }
    }
  }
  
  saveFrame("image2.png");
}

void getBoundaries() {
    topLeft = gShape.getTopLeft();
    bottomRight = gShape.getBottomRight();
    //println("shape boundary - x_min,y_min: "+topLeft.x+", "+topLeft.y+" - x_max,y_max: "+bottomRight.x+", "+bottomRight.y);
}