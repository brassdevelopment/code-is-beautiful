import geomerative.*;
Line[] lines;
int numOfLines = 0;
float radius;
float bigRadius;
color col;
PShape alien;
float spacer = 10.0;
int max = 100;
float xpos = 0.0;
float ypos = 5.0;
color colTwo;
RShape gShape;
color a = #570027;
color b = #EE2A61;
color c = #EF5174;
color d = #F18295;
color e = #8E227E;
color f = #FC3153;
color g = #FF7942;
color h = #FF9F3A;
color j = #FF3254;
color y = #EECB2D;

color aa = #092846;
color bb = #00BCE4;
color cc = #70CCEA;
color dd = #D4ECF7;
color ee = #66BD62;
color ff = #66BD62;

color darkPurp =  #2D183D;
color lightPurp = #580F23;
color midPurp = #562355; 
color bgPurpl = #18183d;
color[] hexs = { a, b, c, d, e, f, g, h, j, aa,bb,cc,dd,ee,ff, y};
color[] blueHexs = { aa,bb,cc,dd,ee,ff, a, b, c };
color[] purpHexs = { darkPurp, darkPurp, midPurp,  darkPurp, bgPurpl, darkPurp, lightPurp };
color[] brassHexs = { bb, cc, dd, ee, ff, b, c, bb, cc, ee, ff, b, c, f, f };

color redOne = #570027;
color redTwo = #EE2A61;
color redThree = #EF5174;
color redFour = #F18295;
color redFive = #F5AEB8;
color redSix = #FAD8DC;
color[] redHues = { redOne, redTwo, redThree, redFour, redFive, redSix, redTwo };

void mouseClicked() {
  saveFrame("image####.png");
}

void setup() {
  //colorMode(HSB, height, height, height);
  smooth();
  size(800, 800);
  background(#18163d);
  noLoop();
  RG.init(this); 
  alien = createShape(GROUP);
  RCommand.setSegmentLength(10);
  RCommand.setSegmentator(RCommand.UNIFORMLENGTH);
  gShape = RG.loadShape("circ.svg");
  numOfLines = 1200;
  radius = 2.0;
  bigRadius = 2.0;
  lines = new Line[numOfLines];
  for (int i = 0; i < numOfLines; i = i+1) {
    lines[i] = new Line();
    lines[i].configure();
  }
}

boolean shapeContains(float xPoint, float yPoint) {
  println(xPoint);
  return gShape.contains(new RPoint(xPoint, yPoint));
}

void draw() {
   
  for (Line line : lines) {
    float xStart = line.xStart;
    float xFinish = line.xFinish;
    float yStart = line.yStart;
    float yFinish = line.yStart;
    strokeWeight(5);
    color col =  brassHexs[ int(random(0, brassHexs.length)) ];
    //strokeCap(ROUND);
    //stroke(col);
    boolean isInShapeStart = gShape.contains(new RPoint(xStart, yStart));
    boolean isInShapeEnd = gShape.contains(new RPoint(xFinish, yFinish));
    if(isInShapeStart && isInShapeEnd) {
      //line(xStart, yStart, xFinish, yFinish);
      PShape head = createShape(LINE, xStart, yStart, xFinish, yFinish);
      head.setStroke(col);
      alien.addChild(head);
    }
    if(isInShapeStart && isInShapeEnd == false) {
      float newXEnd = xFinish - 1.0;
      for (int i = 0; i < max; i = i+1) {
        newXEnd = newXEnd - 1.0;
        if(shapeContains(newXEnd, yFinish)) {
          break;
        }
      }
      PShape enderLine = createShape(LINE, xStart, yStart, newXEnd, yFinish);
      enderLine.setStroke(col);
      alien.addChild(enderLine);
    }
    if(isInShapeStart == false && isInShapeEnd) {
      // extend start
      float newXStart = xStart + 1.0;
      for (int i = 0; i < max; i = i+1) {
        newXStart =newXStart + 1.0;
        if(shapeContains(newXStart, yFinish)) {
          break;
        }
      }
      PShape startLine = createShape(LINE, newXStart, yStart, xFinish, yFinish);
      startLine.setStroke(col);
      alien.addChild(startLine);
    }
  }
  
  translate(260, -150);
  rotate(radians(30));
  shape(alien);
 
}


class Line {
  float yStart;
  float xStart;
  float xFinish;
  
  void configure() {
    yStart = ypos;
    xStart = xpos + spacer;
    float newXFinish = random(xStart, xStart + max);
    if(newXFinish > width) {
      xStart = 0;
      newXFinish = random(xStart, xStart + max);
      yStart = yStart + 12;
    }
    xFinish = newXFinish;
    ypos = yStart;
    xpos = xFinish;
  }
  
}